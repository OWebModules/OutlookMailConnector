﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using OutlookMailConnector.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookMailConnector.Services
{
    public class ElasticServiceAgent
    {
        private Globals globals;

        public async Task<ResultItem<ParseOutlookMessagesModel>> GetParseOutlookMessagesModel(ParseMsgFileRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ParseOutlookMessagesModel>>(() =>
           {
               var result = new ResultItem<ParseOutlookMessagesModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new ParseOutlookMessagesModel()
               };

               var searchConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig,
                       GUID_Parent = Config.LocalData.Class_ParserConfig__Outlook_.GUID
                   }
               };

               var dbReaderConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Config!";
                   return result;
               }

               result.Result.Config = dbReaderConfig.Objects1.SingleOrDefault();

               if (result.Result.Config == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Error while getting the Config!";
                   return result;
               }

               var searchTextParser = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_ParserConfig__Outlook__belonging_Destination_Textparser.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_ParserConfig__Outlook__belonging_Destination_Textparser.ID_Class_Right
                   }
               };

               var dbReaderTextParser = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderTextParser.GetDataObjectRel(searchTextParser);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the TextParser!";
                   return result;
               }

               result.Result.TextParser = dbReaderTextParser.ObjectRels.OrderBy(rel => rel.OrderID).Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = globals.Type_Object
               }).FirstOrDefault();

               if (result.Result.TextParser == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Textparser found!";
                   return result;
               }

               var searchPath = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_ParserConfig__Outlook__belonging_Source_Path.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_ParserConfig__Outlook__belonging_Source_Path.ID_Class_Right
                   }
               };

               var dbReaderPath = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderPath.GetDataObjectRel(searchPath);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Path!";
                   return result;
               }

               result.Result.SourcePath = dbReaderPath.ObjectRels.OrderBy(rel => rel.OrderID).Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = globals.Type_Object
               }).FirstOrDefault();

               if (result.Result.SourcePath == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Path found!";
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public ElasticServiceAgent(Globals globals)
        {
            this.globals = globals;
        }
    }
}

﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookMailConnector.Models
{
    public class ParseMsgFileRequest
    {
        public string IdConfig { get; private set; }

        public OntologyAppDBConnector.IMessageOutput OutputMessage { get; set; }

        public ParseMsgFileRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}

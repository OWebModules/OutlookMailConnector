﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookMailConnector.Models
{
    public class ParseOutlookMessagesModel
    {
        public clsOntologyItem Config { get; set; }
        public clsOntologyItem TextParser { get; set; }
        public clsOntologyItem SourcePath { get; set; }
    }
}

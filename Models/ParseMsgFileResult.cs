﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookMailConnector.Models
{
    public class ParseMsgFileResult
    {
        public long CountParsed { get; set; }
    }
}

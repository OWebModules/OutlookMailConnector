﻿using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookMailConnector.Models
{
    public class GetMailsFetchResult
    {
        public List<MailItem> MailItems { get; set; } = new List<MailItem>();
        public long TotalCount { get; set; }
        public long DoneCount { get; set; }
    }
}

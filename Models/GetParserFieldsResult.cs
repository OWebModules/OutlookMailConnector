﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookMailConnector.Models
{
    public class GetParserFieldsResult
    {
        public List<string> ParserFields { get; set; } = new List<string>();
    }
}

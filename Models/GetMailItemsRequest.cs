﻿using Microsoft.Office.Interop.Outlook;
using OntologyAppDBConnector;
using System.Threading;

namespace OutlookMailConnector.Models
{
    public class GetMailItemsRequest
    {
        public long PageSize { get; set; } = -1;
        public MAPIFolder BaseFolder { get; set; }

        public bool GetSubFolders { get; private set; }
        public bool OnlyUnread { get; set; }
        public IMessageOutput MessageOutput { get; set; }
        public CancellationToken CancellationToken { get; private set; }

        public GetMailItemsRequest(CancellationToken cancellationToken, bool getSubFolders)
        {
            CancellationToken = cancellationToken;
            GetSubFolders = getSubFolders;
        }
    }
}

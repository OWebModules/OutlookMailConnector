﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookMailConnector.Models
{
    public class ExportToMsgFilesRequest
    {
        public string DestPath { get; private set; }
        public bool ExportSubFolders { get; private set; }

        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }

        public ExportToMsgFilesRequest(string destPath, bool exportSubFolders)
        {
            DestPath = destPath;
            ExportSubFolders = exportSubFolders;
        }
    }
}

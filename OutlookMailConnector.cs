﻿using ElasticSearchNestConnector;
using HtmlEditorModule.Converters;
using Microsoft.Office.Interop.Outlook;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using OutlookMailConnector.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OutlookMailConnector
{
    public delegate bool FetchMailItemPageDelegate(GetMailsFetchResult fetchResult);
    public class OutlookMailConnector : AppController
    {
        Microsoft.Office.Interop.Outlook.Application outlookApp;
        Microsoft.Office.Interop.Outlook.NameSpace mapiItem;

        public FetchMailItemPageDelegate FetchMailItemPageEvent;

        private const string parserFieldCreateItem = "CreateTime";
        private const string parserFieldDisplayName = "DisplayName";
        private const string parserFieldSenderAddress = "SenderAddress";
        private const string parserFieldReceiverAddess = "ReceiverAddess";
        private const string parserFieldSubject = "Subject";
        private const string parserFieldBody = "Body";
        private const string parserFieldBodyText = "BodyText";

        public clsOntologyItem IsOutlookRunning
        {
            get
            {
                var result = Globals.LState_Success.Clone();
                try
                {
                    outlookApp = (Microsoft.Office.Interop.Outlook.Application)Marshal.GetActiveObject("Outlook.Application");
                    var objActiveExplorer = outlookApp.ActiveExplorer();
                    if (objActiveExplorer == null)
                    {
                        result = Globals.LState_Error.Clone();
                        result.Additional1 = "Outlook is not running!";
                        return result;
                    }

                    return result;
                }
                catch (System.Exception ex)
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = "Outlook is not running!";
                    return result;
                }



            }
        }

        public async Task<ResultItem<ParseMsgFileResult>> ParseMsgFiles(ParseMsgFileRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ParseMsgFileResult>>(async () =>
            {
                var result = new ResultItem<ParseMsgFileResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new ParseMsgFileResult()
                };


                var elasticAgent = new Services.ElasticServiceAgent(Globals);

                var getModelResult = await elasticAgent.GetParseOutlookMessagesModel(request);
                result.ResultState = getModelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.OutputMessage?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                try
                {
                    if (!Directory.Exists(getModelResult.Result.SourcePath.Name))
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "The Sourcepath does not exist!";
                        request.OutputMessage?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    var textParserController = new TextParserModule.TextParserController(Globals);
                    var textParserResult = await textParserController.GetTextParser(getModelResult.Result.TextParser);
                    result.ResultState = textParserResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.OutputMessage?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    var textParser = textParserResult.Result.FirstOrDefault();

                    if (textParser == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "No Textparser found!";
                        request.OutputMessage?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    var parserFieldsResult = await textParserController.GetParserFields(new clsOntologyItem
                    {
                        GUID = textParser.IdFieldExtractor,
                        Name = textParser.NameFieldExtractor
                    });

                    result.ResultState = parserFieldsResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.OutputMessage?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    var files = Directory.GetFiles(getModelResult.Result.SourcePath.Name, "*.msg", SearchOption.AllDirectories);

                    var docList = new List<clsAppDocuments>();
                    var dbReader = new clsUserAppDBSelector(textParser.NameServer, textParser.Port, textParser.NameIndexElasticSearch, 5000, Globals.Session);
                    var dbWriter = new clsUserAppDBUpdater(dbReader);

                    request.OutputMessage?.OutputInfo($"Found {files.Count()} files");
                    var ix = 0;
                    foreach (var pathMsgFile in files)
                    {
                        using (var msg = new MsgReader.Outlook.Storage.Message(pathMsgFile))
                        {
                            var document = new clsAppDocuments
                            {
                                Id = msg.Id,
                                Dict = new Dictionary<string, object>()
                            };

                            var dateSent = msg.Headers != null ? msg.Headers.DateSent.ToLocalTime() : msg.CreationTime;
                            document.Dict.Add(parserFieldCreateItem, dateSent);
                            document.Dict.Add(parserFieldDisplayName, msg.Sender.DisplayName);
                            document.Dict.Add(parserFieldSenderAddress, msg.Sender.Email);
                            document.Dict.Add(parserFieldReceiverAddess, msg.Headers != null ? string.Join(";", msg.Headers.To) : msg.ReceivedBy.Email ?? "");
                            document.Dict.Add(parserFieldSubject, msg.Subject);
                            if (!string.IsNullOrEmpty(msg.BodyHtml))
                            {
                                var text = HtmlToPlainTextConverter.Convert(msg.BodyHtml);
                                document.Dict.Add(parserFieldBody, msg.BodyHtml);
                                document.Dict.Add(parserFieldBodyText, text);
                            }
                            else if (!string.IsNullOrEmpty(msg.BodyRtf))
                            {
                                var text = "";
                                using (RichTextBox rtb = new RichTextBox())
                                {
                                    rtb.Rtf = msg.BodyRtf;
                                    text = rtb.Text;
                                }
                                document.Dict.Add(parserFieldBody, msg.BodyRtf);
                                document.Dict.Add(parserFieldBodyText, text);
                            }
                            else
                            {
                                document.Dict.Add(parserFieldBody, msg.BodyText);
                                document.Dict.Add(parserFieldBodyText, msg.BodyText);
                            }


                            docList.Add(document);

                        }

                        if (docList.Count % 500 == 0)
                        {
                            result.ResultState = dbWriter.SaveDoc(docList, textParser.NameEsType);

                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                result.ResultState.Additional1 = "Error while writing documents!";
                                return result;
                            }
                            request.OutputMessage?.OutputInfo($"Saved {docList.Count} documents");
                            docList.Clear();
                        }
                        if (ix > 0 && ix % 1000 == 0)
                        {
                            request.OutputMessage?.OutputInfo($"Checked {ix} files");
                        }
                        ix++;
                    }


                    if (docList.Count > 0)
                    {
                        result.ResultState = dbWriter.SaveDoc(docList, textParser.NameEsType);

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while writing documents!";
                            return result;
                        }
                        request.OutputMessage?.OutputInfo($"Saved {docList.Count} documents");
                        docList.Clear();
                    }

                }
                catch (System.Exception ex)
                {

                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Error while parsing the files: {ex.Message}";
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<GetParserFieldsResult>> GetParserFields()
        {
            var taskResult = await Task.Run<ResultItem<GetParserFieldsResult>>(() =>
            {
                var result = new ResultItem<GetParserFieldsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new GetParserFieldsResult()
                };

                result.Result.ParserFields.Add(parserFieldBody);
                result.Result.ParserFields.Add(parserFieldBodyText);
                result.Result.ParserFields.Add(parserFieldCreateItem);
                result.Result.ParserFields.Add(parserFieldDisplayName);
                result.Result.ParserFields.Add(parserFieldReceiverAddess);
                result.Result.ParserFields.Add(parserFieldSenderAddress);
                result.Result.ParserFields.Add(parserFieldSubject);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ExportToMsgFilesResult>> ExportToMsgFiles(ExportToMsgFilesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ExportToMsgFilesResult>>(() =>
           {
               var result = new ResultItem<ExportToMsgFilesResult>
               {
                   ResultState = Globals.LState_Success.Clone()
               };

               request.MessageOutput?.OutputInfo($"Check Outlook-State...");
               result.ResultState = IsOutlookRunning;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Checked Outlook-State.");

               try
               {
                   request.MessageOutput?.OutputInfo($"Export Mailitems...");
                   result.ResultState = ExportMailItems(request);
                   request.MessageOutput?.OutputInfo($"Exported Mailitems.");
               }
               catch (System.Exception ex)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = $"Error while getting the Items: {ex.Message}";
                   return result;
               }
               


               return result;
           });

            return taskResult;
        }

        private clsOntologyItem ExportMailItems(ExportToMsgFilesRequest request, MAPIFolder mapiFolder = null)
        {
            var result = Globals.LState_Success.Clone();
            try
            {
                
                var folderName = request.DestPath;

                request.MessageOutput?.OutputInfo($"Search in Path {folderName}");
                if (mapiFolder != null)
                {
                    folderName = Path.Combine(request.DestPath, mapiFolder.Name);
                }
                else
                {
                    mapiFolder = outlookApp.ActiveExplorer().CurrentFolder;
                }

                if (!Directory.Exists(folderName))
                {
                    Directory.CreateDirectory(folderName);
                }

                
                result.Count = mapiFolder.Items.Count;
                request.MessageOutput?.OutputInfo($"Found {result.Count} MailItems");
                var invalidFileNameChars = System.IO.Path.GetInvalidFileNameChars();
                for (int i = 1; i <= mapiFolder.Items.Count; i++)
                {
                    var item = mapiFolder.Items[i];
                    if (item != null)
                    {
                        string fileNamePre = $"_{item.EntryID}";

                        var fileName = Path.Combine(folderName, $"{fileNamePre}.msg");

                        if (!System.IO.File.Exists(fileName))
                        {
                            item.SaveAs(fileName);
                        }
                        
                        if (i % 100 == 0)
                        {
                            request.MessageOutput?.OutputInfo($"Saved {i} MailItems");
                        }
                    }

                }

                if (request.ExportSubFolders)
                {
                    foreach (Microsoft.Office.Interop.Outlook.MAPIFolder mapiSubFolder in mapiFolder.Folders)
                    {
                        result = ExportMailItems(request, mapiSubFolder);
                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            return result;
                        }
                        result.Count += result.Count;
                    }
                }
                
            }
            catch (System.Exception ex)
            {

                result = Globals.LState_Error.Clone();
                result.Additional1 = $"Error while getting MailItems or SubFolders of {mapiFolder.Name}";
                return result;
            }

            return result;
        }

        public async Task<ResultItem<GetMailsFetchResult>> GetMailItems(GetMailItemsRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<GetMailsFetchResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new GetMailsFetchResult()
                };

                request.MessageOutput?.OutputInfo($"Check Outlook-State...");
                result.ResultState = IsOutlookRunning;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Checked Outlook-State.");

                long pageSize = 100;
                if (request.PageSize != -1)
                {
                    pageSize = request.PageSize;
                }

                MAPIFolder mapiFolder = request.BaseFolder;
                try
                {

                    if (mapiFolder != null)
                    {
                        request.MessageOutput?.OutputInfo($"Search for messages in {mapiFolder.Name}...");
                    }
                    else
                    {
                        mapiFolder = outlookApp.ActiveExplorer().CurrentFolder;
                        request.MessageOutput?.OutputInfo($"Search for messages in {mapiFolder.Name}...");
                    }

                    result.Result.TotalCount = mapiFolder.Items.Count;
                    request.MessageOutput?.OutputInfo($"Found {result.Result.TotalCount} MailItems");
                    for (int i = 1; i <= mapiFolder.Items.Count; i++)
                    {
                        var item = mapiFolder.Items[i] as MailItem;
                        if (item != null)
                        {
                            if (!(request.OnlyUnread && !item.UnRead))
                            { 
                                result.Result.MailItems.Add(item);

                                var mailCount = result.Result.MailItems.Count;
                                if ( mailCount % pageSize == 0)
                                {
                                    request.MessageOutput?.OutputInfo($"Found {mailCount} MailItems");
                                    if (FetchMailItemPageEvent != null)
                                    {
                                        FetchMailItemPageEvent?.Invoke(result.Result);
                                        result.Result.MailItems.Clear();
                                    }
                                }
                            }
                        }
                        result.Result.DoneCount++;
                    }

                    if (request.GetSubFolders)
                    {
                        foreach (Microsoft.Office.Interop.Outlook.MAPIFolder mapiSubFolder in mapiFolder.Folders)
                        {
                            request.BaseFolder = mapiSubFolder;
                            var subFolderResult = await GetMailItems(request);
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                return result;
                            }
                            result.Result.TotalCount += subFolderResult.Result.TotalCount;
                            result.Result.DoneCount += subFolderResult.Result.TotalCount;

                            var mailCount = result.Result.MailItems.Count;
                            if (mailCount % pageSize == 0)
                            {
                                request.MessageOutput?.OutputInfo($"Found {mailCount} MailItems");
                                if (FetchMailItemPageEvent != null)
                                {
                                    FetchMailItemPageEvent?.Invoke(result.Result);
                                    result.Result.MailItems.Clear();
                                }
                            }
                        }
                    }

                }
                catch (System.Exception ex)
                {

                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Error while getting MailItems or SubFolders of {mapiFolder.Name}";
                    return result;
                }

                if (FetchMailItemPageEvent != null && result.Result.MailItems.Any())
                {
                    FetchMailItemPageEvent?.Invoke(result.Result);
                    result.Result.MailItems.Clear();
                }

                return result;
            });

            return taskResult;
        }

        public OutlookMailConnector(Globals globals) : base(globals)
        {
        }
    }
}
